import React from 'react';
import ReactDOM from 'react-dom';
import App from "./caster-app/app";
import combineReducers from './reducers';
import thunk from 'redux-thunk';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware, compose} from 'redux';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

ReactDOM.render(
    <Provider store={createStore(combineReducers, composeEnhancers(applyMiddleware(thunk)))}>
        <App/>
    </Provider>,
    document.querySelector('#root')
)