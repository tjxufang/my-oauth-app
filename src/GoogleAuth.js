import React from 'react';
import {connect} from 'react-redux'
import {signIn, signOut} from "./actions";

class GoogleAuth extends React.Component {
    // state = {isSignedIn: null}

    renderSignIn() {
        if (this.props.isSignedIn === null) {
            return null
        } else if (this.props.isSignedIn === true) {
            return <div>
                <button
                    className="ui red google button"
                    onClick={this.onSignOut}
                >
                    <i className='google icon'></i>
                    Sign Out
                </button>
            </div>
        } else {
            return <div>
                <button
                    className="ui red google button"
                    onClick={this.onSignIn}
                >
                    <i className='google icon'></i>
                    Sign In
                </button>
            </div>
        }
    }

    onSignIn = () => {
        this.auth.signIn()
    }

    onSignOut = () => {
        this.auth.signOut()
    }

    render() {
        return (
            <div>
                Google Auth:
                {this.renderSignIn()}
                <div>UID: {this.props.gid}</div>
            </div>
        )
    }

    componentDidMount() {
        // Google api
        window.gapi.load('auth2', () => {
            window.gapi.auth2.init({
                client_id: '422926594083-eqisj53b48kan1lgfpf3okp3hk4tchlj.apps.googleusercontent.com'
            }).then(() => {
                this.auth = window.gapi.auth2.getAuthInstance()
                this.updateAuthStatus(this.auth.isSignedIn.get())
                this.auth.isSignedIn.listen(this.updateAuthStatus) // listen: refresh when logging-in/out
            })
        })
    }

    updateAuthStatus = isSignedIn => {
        if (isSignedIn){
            this.props.signIn(this.auth.currentUser.get().getId())
        } else{
            this.props.signOut()
        }
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        isSignedIn: state.oauthReducer.isSignedIn,
        gid: state.oauthReducer.gid
    }
}

const mapActionToProps = {signIn, signOut}

export default connect(mapStateToProps, mapActionToProps)(GoogleAuth)
