import {STREAM_CREATE, STREAM_DELETE, STREAM_FETCH, STREAM_FETCH_ALL, STREAM_UPDATE} from "../helper";
import streams from "../api/streams";
import routerHistory from "../routerHistory";

const restObj = '/streams'

export const streamCreate = formData => async dispatch => {
    let res
    try {
        res = await streams.post(restObj, formData)
    } catch (e) {
        return
    }
    dispatch({
        type: STREAM_CREATE,
        payload: res.data
    })
    routerHistory.push('/')
}

export const streamDelete = id => async dispatch => {
    let res = await streams.delete(`${restObj}/${id}`)
    dispatch({
        type: STREAM_DELETE,
        payload: res.data
    })
}

export const streamUpdate = (id, formData) => async dispatch => {
    let res = await streams.put(`${restObj}/${id}`, formData)
    dispatch({
        type: STREAM_UPDATE,
        payload: res.data
    })
}

export const streamFetch = id => async dispatch => {
    let res = await streams.get(`${restObj}/${id}`)
    dispatch({
        type: STREAM_FETCH,
        payload: res.data
    })
}
export const streamFetchAll = () => async dispatch => {
    let res = await streams.get(restObj)
    dispatch({
        type: STREAM_FETCH_ALL,
        payload: res.data
    })
}
