import React, {Component} from 'react';
import EditTitle from "./EditTitle";
// import {connect} from 'react-redux'
// import {Field, reduxForm, SubmissionError} from 'redux-form'

class StreamItem extends Component {
    state = {
        showSmallButton: false,
    }

    whenMouseEnter = evt => {
        this.setState({showSmallButton: true});
    }
    whenMouseLeave = evt => {
        this.setState({showSmallButton: false})
    }

    render() {
        return (
            <li className="media stream-item border-bottom" onMouseEnter={this.whenMouseEnter} onMouseLeave={this.whenMouseLeave}>
                <img
                    src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJcAAACXCAMAAAAvQTlLAAAAllBMVEX///8AZrFvb28AWaylu9ppaWlmZmZsbGz6+vphYWGSkpLCwsK5ubmEhIR9fX16enrm5ubc3Nzx8fFcXFzR0dGenp6np6fKysoAX64AYq+zs7N0dHSLi4vT3+0AUaiYmJjr8fi5y+KYstVrk8UATKklU4MmbLRJa5ORrNNdi8I3drjJ2epMe7p1m8l8osw9fLtSUlJRhMAYVMnTAAAK20lEQVR4nO2c65baug6AQ9i249zIjUASLjMtbaf33fd/uSM5wASwbIeB7jlroR9dbYOdL7IkS44dz3vIQx7ykIc8xEGCIssXcT0PV0mSrMJ5HS/yrAj+W6YqnoczLiXnjAkUxjj+cxbO4+q/YSvyNmUMcfxLQUDG0zYv/jJVVaeSiyOFUGqSSnHiSCq4TOvqrzEFeY3Dtr819zs0qzaOUOIWDa3z+R4aB7bO/8qIRlvJ90wlX7Vg5ee3DcAT2hVc7dm4DKN7QxVxxxnejPEuaY2KCPI2ef1xe09LC9qZFL2mtlHm0CCLtr3WhJy19xrNIOrUPRhL48HTB3nU1tu4/0ce1m10EiGKGNxWPUsX3YUsD0tFJcNTFytm4IV81t+zhr/LMj9tWYVSkZXh2YUbSFErU2EiPB+/IlW4C/V3ZU7i4vZZ6Pc6q29sZtWMKyvRPHExU1xb/HsklZlrYlYeKsvks1vGs6DGcCVYqhuHolPRQKImlOr2ursgS/tO6ptZWbaSytz1UaiAm60SGCLQKgzWFn5JhKtIOYBcuXiyg+SCoWlcGNaBmvlsHkPYL7w5EwmwUVxgZujQ7NL+rpGYK3uPqesZ91ldCBg9sHpZIRf5Wy/GZ/Q5/QNnqRGLp7TuMwlcoCpUmkiDCghburssVf3Vb8XaYjdybrBV4OIt/tl1AjRl4fKCORor376JKgi5Ve1ZiVzeVoWuwst9kivq/VQZBg/f4JbBiqPFm0NOLhU4+iIOD3LpRwkmhv5ChdbPV9eDobZE7z0B2UslfYkOCNOgBCvMOoprzn0+7x8FU0ceXouFtiV8tPgiTmZzQm0L4MIByqoKn4DkykvfL/d9ZL643sbqI1YGiQRknnow5BpcQa655mdFIg7qOoJd5ZWxGkTEQnvg0heJdtKN/i3LAVfRlaXOpmvoYPbaQaaG8oo4liuvwZHJoZTgcSV63V3+cJGdZNJYT15yQRzxh/iD/kdJpmb/SmGBzivMFbrForoyTwlW+1EMFoeHq1SGMnKuDFbg9jJWgKKPFC1MgZzNtLkC+mvw9FQElNuCUahRXCS8OzxaDCpkI6MFWEP/fEUHFq/UVkL6JYX49/wJg/Xzh4+/m81u96ksJUu28eKiQirQF3MvSiEJY92hBwgcvhxl+xgkWQq9BwnzOaooBx+Acbxwtu+/fnxeTpfNZDJpvkB2pcrbLqlPDWeLOUeV9IXb0a+DlOmTSEowBVWuiPOQcpqMC4YDAAniQPPrl8lmOTnI9B9smkf1dlbK0m9fFYtpbIellJx1vujjKfaCTjl0Uptg5FI4Le+DZJaCtlAFoRDHaLj+OplOBqK41C2LvPal9A8ZW9DXwZBZtgvml+p/Y9UNxiL3KIYuzPCpolJNxRAFWR8kgPPg7Os/m2Yy0XIpqeZCln1qmnUq6fKh6JgJ1XGWlD1OyNyDRQA/Fizbc4m0KBLW2z6MB0v6H/1slpOJiQtuXoOfqLtnCWdynqnYgNNt7B+9HR6YOaYWSLNPhRfYzF+xHhOdPVXW8HxJdcmlijvpq8Ay32I7mIugbsoTlYGtlJoiICydli+CDowx3f+jKtEDeqeBiVCoqid4WTaXWBouLIIkf83GoIcyU6k0k/FeSVBCic5FYS1GleOQ5zjBqoR9AdpSoefp21RDpedCD5bpweNgQluhsoScH50wx0hpSnD3gjGCDVKjDIMO+GIOKUXZY2nGkOaCG8ukd8xIzTxYxAwnDbRmh1iBs4M/9JBspowUHR27X+90Y2jg8oqVVA/UF+ZY2Z5yi70PmKU7VReCoUFwNZEA1oTCIrlg0pD4SEFfw3TnYQEV1tmwUNfybAosEn6YPp5IbRm4sAZSGltxXR2KKRC3uCTGLnaReBcz3jvz0zcay8CFGsOksvATXV6D99yaXRK9QzOTBqHCCiiTt3FB3iQx9OjNG7MEaQ76NX+NXUMplJ5f9AHCgcsrpKSDAcQwyyzJTUn3sxHLzAVpLydSyv30bWqMGi3JWNIYjMvKhakmdQnTRmMeVrP9wp9OfpqMy87lpXRuinmjYSBxqZT02LVZW3YumDGoKiM6ZgT6lphXUm3/WNRl5YKRXBFXoBg2eSRM2SIhrq03Fiw7V9FHV51gMW72V+rqV9sw2rlAYdRaSUvEJyXBYRLUyNpG5cKVESV7v6jCqJCvlrCIi+aQ6sjlzangiiohI0UMEztlmXZ1uXBVZAxbCXq5eM5I8/putXonLo+0E6wIdctTHk7Owqfmil+2IOHIVVOxFbN0oi5S+aT+cYIftxlHsO9Ef/PcJ7PpjJFRdf35RlzFjLgDRlamv5TjkqD+aZ4dhtGJK9hKvaUEEFmJiI/1IeGOH+xRwo0LDIzwOnBIAhnLaSIef7yVvrxIEl6HTqdHrmlX/W2dhFy5cklMNxikiHV/MnwFLlhuXAUnphs6gEFZQmgycIiqjlwek3ousKLLQkwJWp4+KQx2t+MSpZ4rIr0uIbmgmp3aZefERdUPyKXP/Qxcn778cyv58unpZlxQr2j//0z+2TlotdmN5TLYVymcuFyi72Q31r4M/ijZ7bg2Y/3REL8Yd1lkd+OajI1fhnifWJY1RnA1v/WN6XhvmB+3hOFdwbX8qG9Mz4+GfCJ2er3kxDX9oG9M5xOG/GshLetm7lzLZ21bQ/5lyFezzuXtkhPX5zVxBzJfNeX3CVXHjOVqfozO7031EFnHjOVa/tK3NdRDpvoxd5mJXLg23/VtDfWjsd4+2SPxBq4J0dZUb5vWJ1oqLx/HNX3RNzWuTwSMXs/JHLYUuOhL741qPYdSiXn9K7Rbvp2r+Uo0Na5/GdcLs1LaQpida0Ooy7JeaFxftSvMyrX8Q7S0rK8a16MzYXtBbtcXpS7LerR5/b6mSlJXruVPqqVl/d7yvsO2acXC1TRUQ+v7DvP7oQVnxpG0cE31mYTn8H6IfJ/WSyuNPmnmokKq5/I+jXj/eOxAmvZFGbmW3+idk/b3j/r3tUcpEhOYiav59kS2c3hfq32/PRCIrvTeXxMXUc2qPh3eb+v2A5x24tMao7kaMnJ5jvsBLvdPnIOVckUYP8nV7AxYjvsnzvebXIIlkjBSimtpsC3n/SZn+3N05KnUB2eCa2rEct2fc7qfSS8tl7qjHlquZvlidDXn/UzD/V+ULHypOXKj41o2ZJTvb+a8/2uwX84gtZCyPvvNJdeyIafqXsbsl8MdvmbTVz2GpRSne8zPuZrNH4MfKsH9heZJdyi1cfp+JcNNl3V+PEx7yjWdfLVRjdyP+bp/1UbW+qUsZ9s6UgdWB1zLzeTFSjV6/6qaSUXqMux5nXTqACsYyhe163c5XX7+8YsoX08kwONZY/b7DvZHO/SeLeJ5wmRZftrtNs3vjx+e126GPH5/9GA/uWuDICiensj95Dq5Zj/5YP/9veSq/fdXnye4f/+v5zvuIVef7xieh7m9vOE8zPD80K3lTeeHTs5b3VTeeN7K7XzaeHnz+TSn83yj5e3n+TyX848j5SbnHz2H86Lj5FbnRfdqF/T52nGd3ex8rfU88gi56Xlk2/ltd7nx+W3LeXdXuf15d8/4fQBHqnt8H8AzfU/BRe72PQXv+MQX35+wyz2/P+GR3+uwyN2/1+GdfN8kfD/fN0G5+nswIzR8pbzL7+eo277P7w0peY/fZ+oFv2fF3933rHp5j9//OrK9w++lPeQhD3nIQ/5v5H8saKeCPHjoUAAAAABJRU5ErkJggg=="
                    width="50px" className="mr-3" alt="..."/>
                <div className="media-body">
                    <EditTitle stream={this.props.stream}/>
                    {this.props.stream?.description}
                </div>
                <div style={{visibility: this.state.showSmallButton ? 'visible' : 'hidden'}}>
                    <i className="icon-button-group fas fa-edit"></i>
                    <i className="icon-button-group fas fa-trash-alt"></i>
                </div>
            </li>
        )
    }
}


export default StreamItem;