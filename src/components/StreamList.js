import React, {Component} from 'react'
import {connect} from 'react-redux'
import {Field, reduxForm, SubmissionError} from 'redux-form'
import {streamFetchAll} from "../actions/streams";
import StreamItem from "./StreamItem";
import {Link} from "react-router-dom";


class StreamList extends Component {
    constructor() {
        super()
    }

    componentDidMount() {
        this.props.streamFetchAll()
    }

    renderStreamList() {
        return (
            <div>
                {this.props.streams.map(stream => <StreamItem stream={stream}/>)}
            </div>
        )

    }

    render() {

        return (
            <div>
                <ul className="list-unstyled">
                    {this.renderStreamList()}
                </ul>
                <Link type="button"
                      to="/stream/create"
                      class="btn btn-outline-primary" style={{display: this.props.isSignedIn ? 'block' : 'none'}}>Create</Link>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        streams: state.streamReducer,
        isSignedIn: state.oauthReducer.isSignedIn
    }
}

const formWrapped = reduxForm({})(StreamList)

export default connect(mapStateToProps, {streamFetchAll})(formWrapped)