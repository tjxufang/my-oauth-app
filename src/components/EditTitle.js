import React, {Component} from 'react';

class EditTitle extends Component {
    state = {showInput: false}
    hShowElement = (edit) => {
        return {display: edit ? 'block' : 'none'}
    }

    whenDoubleClick = () => {
        this.setState({showInput: !this.state.showInput})
    }

    render() {
        return (
            <div>
                <h5 className="mt-0 mb-1" onDoubleClick={this.whenDoubleClick}>
                    <div style={this.hShowElement(this.state.showInput)}>
                        <input type="text" value={this.props.stream?.title}/>
                    </div>
                    <div style={this.hShowElement(!this.state.showInput)}>{this.props.stream?.title}</div>

                </h5>
            </div>
        );
    }
}



export default EditTitle;