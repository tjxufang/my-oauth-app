import React, {Component} from 'react'
import {connect} from 'react-redux'
import {Field, reduxForm, SubmissionError} from 'redux-form'
import {streamCreate} from "../actions/streams";

// const delayPost = ms => new Promise((resolve) => setTimeout(resolve, ms))
class StreamCreate extends Component{
    renderInput = ({input, label, type, meta: {touched, error}}) => (
        <div>
            <label>{label}</label>
            <div>
                <input type={type} {...input} placeholder={label}/>
                {touched && error && <span>{error}</span>}
            </div>
        </div>
    )

    FORM_FAILED = 'Login Failed'

    submitForm = (formData) => {
        // console.log(formValues)
        // return delayPost(2000).then(() => {
        //     if (!['jerry', 'curry', 'pan', 'jessie'].includes(formValues.username)){
        //         throw new SubmissionError({
        //             username: 'User odes not exist', // for name="username"
        //             _error: this.FORM_FAILED // global error
        //         })
        //     } else if (formValues.password !== 'mark2win'){
        //         throw new SubmissionError({
        //             password: 'Wrong password',
        //             _error: this.FORM_FAILED
        //         })
        //     } else {
        //         console.log(`Login successfully, ${formValues}`)
        //     }
        // })
        this.props.streamCreate(formData)
    }

    render() {
        const {error, handleSubmit, pristine, reset, submitting} = this.props
        return (
            <div>
                <div>Stream Create</div>
                <form onSubmit={handleSubmit(this.submitForm)}>
                    {error && <strong>{error}</strong>}
                    <Field name="title"
                           type='text'
                           label='Stream Title'
                           component={this.renderInput}/>
                    <Field name="description"
                           type='text'
                           label='Description'
                           component={this.renderInput}/>
                    <div>
                        <button type='submit' disabled={submitting}>Create</button>
                        <button disabled={pristine || submitting}>Clear All</button>
                    </div>
                </form>
            </div>
        )
    }
}

const validate = values => {
    const errors = {}
    if (!values.title) {
        errors.title = 'Required'
    } else if (values.title.length > 15){
        errors.title = 'Must be 15 characters or less'
    }
    return errors
}

const formWrapped = reduxForm({
    form: 'streamCreate',
    validate,
})(StreamCreate)

export default connect(null, {streamCreate})(formWrapped)