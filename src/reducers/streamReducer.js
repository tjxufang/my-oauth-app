import {STREAM_CREATE, STREAM_DELETE, STREAM_FETCH_ALL, STREAM_UPDATE} from "../helper";

export const streamReducer = (state=[], action) => {
    switch (action.type) {
        case STREAM_CREATE:
            return [action.payload]
        case STREAM_DELETE:
            return state.filter(stream => stream.id !== action.payload)
        case STREAM_UPDATE:
            return state.map(stream => stream.id === action.payload.id ? action.payload : stream)
        case STREAM_FETCH_ALL:
            return [...state, ...action.payload]
        default:
            return state
    }
}