import {SIGN_IN, SIGN_OUT} from "../helper";
import {combineReducers} from "redux";
import {reducer as formReducer} from "redux-form"
import {streamReducer} from "./streamReducer";

const initState = {
    isSignedIn: null,
    gid: null
}

const oauthReducer = (state = initState, action) => {
    switch (action.type) {
        case SIGN_IN:
            return {...state, isSignedIn: true, gid: action.payload}
        case SIGN_OUT:
            return {...state, isSignedIn: false, gid: null}
        default:
            return state
    }
}

export default combineReducers(
    {
        oauthReducer,
        form: formReducer,
        streamReducer
    }
)